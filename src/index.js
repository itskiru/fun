'use strict';

const path = require('path');

class Fun {
	constructor(base, api) {
		this.base = base;
		this.api = api;

		const { version } = require('../package.json');
		this.version = version;

		this.register = null;
	}

	async onMount() {
		this.register = await this.api.Commands.registerAll(path.resolve(__dirname, 'Commands'));
	}

	async onUnmount() {
		await this.register.unregisterAll();
	}
}

module.exports = Fun;
